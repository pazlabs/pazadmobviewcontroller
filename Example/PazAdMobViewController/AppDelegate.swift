//
//  AppDelegate.swift
//  PazAdMobViewController
//
//  Created by hpbl on 01/12/2018.
//  Copyright (c) 2018 hpbl. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Firebase
import PazAdMobViewController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.startServices()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    // MARK: Services
    func startServices() {
        FirebaseApp.configure()
        
        self.startAdMob()
        self.loadStoryboard()
    }
    
    // MARK: AdMob
    func startAdMob() {
        // TODO: ADD ApplicationID
        let applicationID = ""

        guard applicationID != "" else {
            fatalError("ADD ApplicationID")
        }
        
        GADMobileAds.configure(
            withApplicationID: applicationID
        )
    }
    
    
    lazy var bannerVC: PazAdMobViewController = {
        let storyBoard = UIStoryboard(
            name: "Main",
            bundle: nil
        )
        
        let contentVC = storyBoard.instantiateViewController(
            withIdentifier: "ContentVC"
        )
        
        var bannerVC = PazAdMobViewController(
            contentViewController: contentVC,
            adUnitId: ""
        )
        
        bannerVC.active = true
        
        return bannerVC
    }()
    
    func loadStoryboard() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.rootViewController = self.bannerVC
        self.window!.makeKeyAndVisible()
        
        // TODO: when releasing change this to valid adUnit
        var adUnit = "ca-app-pub-3940256099942544/2934735716"

        self.bannerVC.set(adUnitId: adUnit, active: true)
    }

}

