//
//  PazAdMobViewController.swift
//  PazAdMobViewController
//
//  Created by Hilton Pintor Bezerra Leite on 12/01/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import UIKit
import GoogleMobileAds

open class PazAdMobViewController: UIViewController {
    
    public enum UpdateNotification {
        case BannerViewActionWillPresent
        case BannerViewActionDidDismiss
        case BannerViewClassBannerSizeChanged
        case BannerViewClassAdUnitIdChanged
        
        var name: Notification.Name {
            switch self {
            case .BannerViewActionDidDismiss:
                return Notification.Name("BannerViewActionDidFinish")
            case .BannerViewActionWillPresent:
                return Notification.Name("BannerViewActionWillBegin")
            case .BannerViewClassAdUnitIdChanged:
                return Notification.Name(
                    "BannerViewClassAdUnitIdChanged"
                )
            case .BannerViewClassBannerSizeChanged:
                return Notification.Name(
                    "BannerViewClassBannerSizeChanged"
                )
            }
        }
    }
    
    private (set) var bannerLoaded: Bool = false
    private (set) var showing: Bool = false
    
    // The MoPub Ad Unit Id to be used to load adverts
    public var adUnitId: String {
        didSet {
            guard self.adUnitId != oldValue else { return }
            
            self.removeAdView()
            
            if self.active {
                self.loadAd()
            }
            
            NotificationCenter.default.post(
                name: UpdateNotification.BannerViewClassAdUnitIdChanged.name,
                object: self
            )
        }
    }
    
    // Activates/Deactivate the presentation of adverts
    public var active: Bool = true {
        didSet {
            if oldValue == self.active { return }
            
            if self.active {
                self.loadAd()
            } else {
                self.removeAdView()
            }
        }
    }
    
    // Ad Size for current device
    public var adSize: GADAdSize {
        didSet {
            guard self.adSize.size != oldValue.size else { return }
            
            if self.adSize.size == CGSize.zero {
                self.removeAdView()
                let _ = self.adView
            }
            
            NotificationCenter.default.post(
                name: UpdateNotification.BannerViewClassBannerSizeChanged.name,
                object: self
            )
        }
    }
    
    
    // MARK: - AdView
    func newAdView() -> GADBannerView {
        let bannerView = GADBannerView(adSize: self.adSize)
        bannerView.adUnitID = self.adUnitId
        
        bannerView.rootViewController = self

        return bannerView
    }
    
    private var _adView: GADBannerView?
    
    public var adView: GADBannerView? {
        guard self.active else { return nil }
        
        if let adView = self._adView { return adView }
        
        let adView = self.newAdView()
        adView.delegate = self
        
        self.contentView.addSubview(adView)
        self._adView = adView
        
        return adView
    }
    
    func removeAdView() {
        guard self._adView != nil else {
            self.bannerLoaded = false
            return
        }
        
        self._adView?.removeFromSuperview()
        self._adView = nil
        self.bannerLoaded = false
        
        UIView.animate(withDuration: 0.25) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    
    func loadAd() {
        let request = GADRequest()
        self.adView?.load(request)
    }
    
    
    // MARK: - Object lifecylce
    public init(contentViewController: UIViewController,
                adUnitId: String,
                active: Bool = true) {
        
        self.adUnitId = adUnitId
        self.active = active
        self.contentViewController = contentViewController
        
        self.adSize = (UI_USER_INTERFACE_IDIOM() == .pad)
            ? kGADAdSizeLeaderboard
            : kGADAdSizeBanner
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Setting everything in one function
    public func set(adUnitId: String, active: Bool = true) {
        self.adUnitId = adUnitId
        self.active = active
    }
    
    // MARK: - View Controller
    public var contentViewController: UIViewController
    public lazy var contentView: UIView = {
        let view = UIView(frame: UIScreen.main.bounds)
        return view
    }()
    
    open override func loadView() {
        super.loadView()
        // Setup containment of the _contentController.
        self.addChildViewController(self.contentViewController)
        self.contentView.addSubview(self.contentViewController.view)
        self.contentViewController.didMove(toParentViewController: self)
        
        self.view = self.contentView
    }

    
    // MARK: - View Updates
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.refreshLayout()
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.refreshLayout()
    }
    
    public func refreshLayout() {
        var contentFrame = self.view.bounds
        guard var bannerFrame = self.adView?.frame else {
            return
        }
        
        // Check if the banner has an ad loaded and ready for display.  Move the banner off
        // screen if it does not have an ad.
        if self.bannerLoaded {
            var offset = bannerFrame.size.height
            
            if #available(iOS 11.0, *) {
                offset += self.view.safeAreaInsets.bottom
            }
            
            contentFrame.size.height -= offset
            bannerFrame.origin.y = contentFrame.size.height
            
            self.showing = true
            
        } else {
            bannerFrame.origin.y = contentFrame.size.height
            
            self.showing = false
        }
        
        self.contentViewController.view.frame = contentFrame
        
        if let nv = self.contentViewController as? UINavigationController {
            nv.visibleViewController?.view.setNeedsLayout()
        }
        
        // Center banner frame
        // Repositioning example: keep the ad view centered horizontally.
        bannerFrame.origin.x =
            (self.view.bounds.size.width - bannerFrame.size.width) / 2
        self.adView?.frame = bannerFrame
    }
    
    // MARK: - Interface Orientation and Rotation
//    open override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
//        if toInterfaceOrientation.isLandscape {
//            self.adView?.adSize = kGADAdSizeSmartBannerLandscape
//        
//        } else {
//            self.adView?.adSize = kGADAdSizeSmartBannerPortrait
//        }
//    }
    
    open override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    open override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return self.contentViewController.preferredInterfaceOrientationForPresentation
    }
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return self.contentViewController.supportedInterfaceOrientations
    }
}


// MARK: - GADBannerViewDelegate
extension PazAdMobViewController: GADBannerViewDelegate {
    public func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        self.bannerLoaded = true
        
        UIView.animate(withDuration: 0.25) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    public func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        guard let _ = self._adView else {
            self.bannerLoaded = false
            return
        }
        
        self.bannerLoaded = false
        
        UIView.animate(withDuration: 0.25) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    public func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        NotificationCenter.default.post(
            name: UpdateNotification.BannerViewActionWillPresent.name,
            object: self
        )
    }
    
    public func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        NotificationCenter.default.post(
            name: UpdateNotification.BannerViewActionDidDismiss.name,
            object: self
        )
    }
}

