# PazAdMobViewController

[![CI Status](http://img.shields.io/travis/hpbl/PazAdMobViewController.svg?style=flat)](https://travis-ci.org/hpbl/PazAdMobViewController)
[![Version](https://img.shields.io/cocoapods/v/PazAdMobViewController.svg?style=flat)](http://cocoapods.org/pods/PazAdMobViewController)
[![License](https://img.shields.io/cocoapods/l/PazAdMobViewController.svg?style=flat)](http://cocoapods.org/pods/PazAdMobViewController)
[![Platform](https://img.shields.io/cocoapods/p/PazAdMobViewController.svg?style=flat)](http://cocoapods.org/pods/PazAdMobViewController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.
Then Follow the required //TODO: comments.

## Requirements

## Installation

PazAdMobViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PazAdMobViewController'
```

## Author

hpbl, hiltonpintor@gmail.com
pazlabs, info@paz-labs.com

## License

PazAdMobViewController is available under the MIT license. See the LICENSE file for more info.
